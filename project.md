# Project Notes: [PROJECT NAME]

Live Domain: [URL HERE]

Staging Domain: [URL HERE]

## Hosting

Hosting: Digital Ocean/Serverpilot [IP ADDRESS HERE]

The site is built with Craft 3.

Forms on the site are handled by the [PLUGIN NAME] plugin.

SEO is handled by the [PLUGIN NAME] plugin.

Emails are handled via [SERVICE NAME].

SSL certificate TBC

## Front-end set-up

See [project readme](README.md) for front-end set-up.

## Plugins

List plugins in use here

## Bespoke functionality

Outline any bespoke functionality in use, usually within the Mud Module.

## Deployment notes

Outline any issues or notes relating to deployment here.

## Issues

Summarise here any idiosyncracies or peculiarities to this prokect that would be useful to other developers.