<?php
	return [
    'metaTemplate' => '_seo/meta',
    'title' => array(
    	['key'      => '1',
			'template' => '{title}',
			'locked'   => false,
		],
		[
			'key'      => '2',
			'template' => ' - {{ siteName }}',
			'locked'   => true,
		]
    ),
    'robotsTxt' => <<<EOT
{# Sitemap URL #}
Sitemap: {{ url(seo.sitemapName ~ '.xml') }}

{# Disallows #}
{% if craft.app.config.env not in ['live','production'] %}

{# Disallow access to everything when NOT in production #}
User-agent: *
Disallow: /

{% else %}

{# Disallow access to cpresources/ when live #}
User-agent: *
Disallow: /cpresources/

{% endif %}
EOT
	];