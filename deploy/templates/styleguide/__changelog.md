# 1.0.1

## Core Styling

- Updated Header and Nav styling (as was previously white on white)
- Rejigged Styleguide templates into separate directories to keep neater.
- Started to add base styling for Styleguide
- Removed need for `section_inner`, ...remoiving it is a WIP. Need to confirm 'best' way to show a lot of text content.

<!-- ## Craft Plugins

// - Added Snaptcha -->

# -----------

# 1.0.0

## Styleguide Templates

- Added styleguide template directory
- Set {{ namespace }} and {{ menuLinks }} as variables, to be overwritten from sub template
  -- Namespace is used for some custom styleguide styling and avoid overwriting actual project code
  -- `styleguide.index` overwrites menuLinks to navigate the styleguide
- Styleguide templates now extending `_layout.twig` (not embedding)
- Added `styleguide_section` and `styleguide_inner` blocks:
  -- `styleguide_section` is wrapper for entire section
  -- `styleguide_inner` is used via an embed of `styleguide/_section.twig` on the child page level, that has general section details (`with` the embed) and the pages' actual content
- Added base form markup

## SCSS

- Added `.o-content` as CMS wrapping class
- Added `%focus` in variables (for forms)
- Added `error` and `success` colors for forms (to abstract?)
- Added base form styling

## Objects

- Added `_objects/button` - renders a link or button based on vars

## Forms

- Added default markup, styling and JS. Detailed comments in each file

## Tailwind

- Moved Colours to overwrite and not extend default colors
- Removed fontSizes (to keep responsive from base.scss)

## JS

- Updated yarn - upgrade-interactive - and GSAP specifically
- Added `FormValidation.js`, and updated `validator.js`
- Updated barba.js to `try` (app.js)
