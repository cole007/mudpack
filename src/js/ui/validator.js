import validate from 'validate.js';
import { eventBus } from '@spon/plugins';
import domEvents from '@spon/domevents';
import debounce from '@/utils';

/**
 * @module utils/validator
 */

/**
 * @property {HTMLElement} props.form
 * @property {Object} props.rules
 * @property {String} props.namespace
 * @return {validatorType}
 */

/**
 * @typedef {Object} validatorType
 * @property {function} validator.destroy - destroy the validator, remove events
 * @property {function} validator.on - listen to events
 * @property {function} validator.off - remove event listeners
 * @property {boolean} validator.isSubmitting - set the isSubmitting state
 */

function validator({ form, rules = {}, namespace, fullMessages = true }) {
	const { addEvents, removeEvents } = domEvents(form);
	const onHandles = [];

	validate.validators.checked = (value, options) => {
		if (value !== true) return options.message || 'must be checked';
	};

	let isSubmitting = false;

	const required = [...form.querySelectorAll('[required]')].reduce(
		(acc, field) => {
			const { name, type } = field;

			// Get custom rule message
			const messageRule = field.dataset.presence
				? {
						message: field.dataset.presence,
				  }
				: true;

			// Add basic message to rules, then..
			const rule = rules[name] ? rules[name] : { presence: messageRule };

			// https://validatejs.org/
			// TODO: Update how rules are added

			// .. update if type is email
			if (type === 'email') {
				rule.email = ''; // Need to overwrite specific email (ie not presence) message
				rule.format = {
					pattern: validate.validators.email.PATTERN,
					message: value =>
						validate.format('^%{num} is not a valid email address', {
							num: value,
						}),
				};
			}

			// .. update if type is phone
			if (type.toUpperCase() === 'TEL') {
				rule.format = {
					pattern: '[0-9]+',
					message: value =>
						validate.format('^%{num} is not a valid phone number', {
							num: value,
						}),
				};
			}

			// .. update if type is select
			if (field.tagName.toUpperCase() === 'SELECT') {
				rule.presence = {
					...rule.presence,
					allowEmpty: false,
				};
			}

			// .. update if type is checkbox
			if (field.type.toUpperCase() === 'CHECKBOX') {
				rule.presence = {
					...rule.presence,
					allowEmpty: false,
				};
				rule.inclusion = {
					within: [true],
					message: field.dataset.presence,
				};
			}

			return {
				...acc,
				[name]: { field, type, rule, name, touched: false, error: false },
			};
		},
		{}
	);

	const getRules = Object.entries(required).reduce((acc, [key, value]) => {
		const { rule } = value;
		return { ...acc, [key]: rule };
	}, {});

	/**
	 * @function onSubmit
	 * @memberof validator
	 * @param {Event} e the event object
	 * @return {void}
	 */
	function onSubmit(e) {
		e.preventDefault();

		if (isSubmitting) {
			return;
		}

		// @ts-ignore
		const fails = validate(e.target, getRules, { fullMessages });

		if (!fails) {
			isSubmitting = true;
			eventBus.emit(`@${namespace}:submit/valid`, e);
			return;
		}

		const errors = Object.entries(fails).map(([key, error]) => {
			const item = { ...required[key], error };

			eventBus.emit(`@${namespace}:input/error`, item);

			return item;
		});
		isSubmitting = false;
		eventBus.emit(`@${namespace}:submit/error`, errors);
	}

	/**
	 * @function onBlur
	 * @memberof validator
	 * @param {Event} e the event object
	 * @return {void}
	 */
	function onBlur(e) {
		// // If we're changing, only continue if it's a clickable thing
		// const checkBlurElement = ['checkbox', 'radio', 'select'];
		// if (e.type === 'change' && checkBlurElement.indexOf(element.type) < 0) {
		// 	return;
		// }

		// pesky blur due to faux inputs
		// const checkBlurElement = ['checkbox', 'radio'];
		// if (e.type === 'blur' && checkBlurElement.indexOf(element.type) > -1)
		// 	return;

		// @ts-ignore
		const { value, name, checked } = e.target;
		const touched = value.length > 0;

		// we only want to start validating
		// once the user has actually interacted
		// with the form.
		if (!touched) return;

		// length check used because validate.js doesn't check empty strings
		// see important notice https://validatejs.org/#overview
		const failed =
			validate.single(checked || value, required[name].rule) ||
			value.length < 1;

		// no errors... emit and return
		if (!failed) {
			Object.assign(required, {
				[name]: {
					...required[name],
					touched,
					error: null,
				},
			});
			eventBus.emit(`@${namespace}:input/valid`, required[name]);
			return;
		}

		// create a fallback message
		// this will only be used if the value.length < 1
		const message = failed === true ? ['DEFAULT_MISSING_MESSAGE'] : failed;

		Object.assign(required, {
			[name]: {
				...required[name],
				touched,
				error: message,
			},
		});

		eventBus.emit(`@${namespace}:input/error`, {
			...required[name],
			error: message,
		});
	}

	addEvents({
		submit: onSubmit,
		'keyup [required]': [debounce(onBlur, 500), true],
		'blur [required]': [onBlur, true],
		'change [required]': [onBlur, true],
	});

	// form.setAttribute('novalidate', true);

	return {
		destroy() {
			removeEvents();
			onHandles.forEach(([event, fn]) => {
				eventBus.off(`@${namespace}:${event}`, fn);
			});

			form.removeAttribute('novalidate');
		},

		on(event, fn) {
			eventBus.on(`@${namespace}:${event}`, fn);
			onHandles.push([event, fn]);
		},

		off(event, fn) {
			eventBus.off(`@${namespace}:${event}`, fn);
		},

		set isSubmitting(value) {
			isSubmitting = value;
		},
	};
}

export default validator;
