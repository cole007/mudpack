import { gsap } from 'gsap';
import axios from 'axios';
import domify from 'domify';
import { insertAfter } from '@/utils';

/*
 *
 * 		Global Message Handling
 *
 */

/*
 * 		Remove all messages
 */
export function removeAllMessages(node) {
	[...node.querySelectorAll('form .o-message')].forEach(old => old.remove());
}

/*
 * 		Error Message Markup
 */
export const errorMessage = message => `
	<p class="mb-0 o-message o-error">${
		typeof message === 'string' ? message : 'Error. Please try again.'
	}</p>
`;

/*
 * 		Success Message Markup
 */
export const successMessage = message => `
	<p class="mb-0 o-message o-success">${
		typeof message === 'string'
			? message
			: 'We will be in touch as soon as we can.'
	}</p>`;

/*
 * 		Add Form Message
 */
export const showMessage = (tl, node, message = null, state = 'error') => {
	const button = node.querySelector('[data-submit]');

	tl.to(node, {
		onComplete: () => {
			removeAllMessages(node);

			if (state === 'success') {
				insertAfter(domify(successMessage(message)), button);
			} else {
				insertAfter(domify(errorMessage(message)), button);
			}
		},
	});
};

/*
 *
 * 		Field Message Handling
 *
 */

/*
 * 		Find Field Note
 */
export const getFieldNote = field => {
	const wrapper = field.closest('.o-field');
	const note = wrapper.querySelector('.o-field_note') || null;
	return note;
};

/*
 * 		Update Field on invalid
 */
export const onInputError = ({ field, error }) => {
	field.classList.add('has-error');
	const wrapper = field.closest('.o-field');
	const label = wrapper.querySelector('.o-label_text');
	const message =
		getFieldNote(field) ||
		insertAfter(domify('<span class="o-field_note"></span>'), label);
	if (message) {
		message.innerHTML = `* ${error} *`;
		gsap.to(message, {
			opacity: 1,
			duration: 0.4,
		});
	}
};

/*
 * 		Update Field on Valid
 */
export const onInputValid = ({ field }) => {
	field.classList.remove('has-error');
	const message = getFieldNote(field);
	if (message) {
		gsap.to(message, {
			opacity: 0,
			duration: 0.1,
			onComplete: () => message.remove(),
		});
	}
};

/*
 *
 * 		Submitting
 *
 */

/*
 * 		Submitting 'gif' markup
 */
const submittingMarkup = `<div class="o-submitting z-40 opacity-0 absolute inset-0 w-full h-full flex justify-center items-center z-50">
		<h1 class="mb-0">Submitting</h1>
	</div>`;

/*
 * 		Form is Submitting
 */
export const isSubmitting = (tl, node) => {
	// Add the message / gif
	const submitting =
		typeof submittingMarkup === 'string'
			? domify(submittingMarkup)
			: submittingMarkup;
	node.append(submitting);

	// Fade form
	tl.to(node, {
		opacity: 0.1,
		duration: 0.3,
		ease: 'linear',
		onStart: () => {
			// housekeeping
			node.classList.add('is-submitting');
			node.setAttribute('disabled', true);
		},
	});

	tl.to(submitting, {
		duration: 0.8,
		opacity: 1,
		delay: 0.3,
	});

	return tl;
};

/*
 * 		Removed Form Submitting
 */
export const removeSubmitting = (tl, node, success = 0) => {
	// Hide / remove submitting message
	const submitting = [...node.querySelectorAll('.o-submitting')];
	if (submitting.length) {
		tl.to(submitting, {
			opacity: 0,
			duration: 0.3,
			ease: 'linear',
			onComplete: () => {
				submitting.forEach(overlay => overlay.remove());
			},
		});
	}

	// Show form
	tl.to(node, {
		opacity: 1,
		duration: 0.5,
		ease: 'linear',
		onStart: () => {
			if (success) {
				node.reset();
			}
		},
		onComplete: () => {
			// housekeeping
			node.classList.remove('is-submitting');
			node.removeAttribute('disabled');
		},
	});

	return tl;
};

/*
 *
 * 		Snaptcha
 *
 */

/*
 * 		Refresh Snaptcha (on all forms)
 *		Todo: Make better
 */
export function updateSnaptcha(form) {
	axios({
		url: '/actions/snaptcha/field/get-field',
		headers: {
			Accept: 'application/json',
		},
	})
		.then(({ data }) => {
			if (data.name && data.value) {
				[...form.querySelectorAll(`input[name="${data.name}"]`)].forEach(
					input => {
						input.value = data.value; // eslint-disable-line
					}
				);
			}
		})
		.catch(err => console.error(err)); // eslint-disable-line
}

/*
 *
 * 		Submit function
 *
 */
export function onSubmitValid(e, form, validate) {
	e.preventDefault();

	if (form.classList.contains('is-submitting')) return;

	// One timeline for whole flow
	const tl = gsap.timeline({});

	isSubmitting(tl, form);

	const method = form.getAttribute('method') || 'post';
	const url = form.querySelector('[name="action"]')
		? form.querySelector('[name="action"]').value
		: window.location.href;
	const data = new FormData(form);

	axios({
		url,
		// timeout: 200, // Test error messages... DO NOT LEAVE IN PRODUCTION!
		method,
		headers: {
			'Access-Control-Allow-Origin': '*',
			'X-Requested-With': 'XMLHttpRequest',
			HTTP_X_REQUESTED_WITH: 'XMLHttpRequest',
		},
		data,
	})
		.then(({ data, status }) => {
			validate.isSubmitting = false; // eslint-disable-line

			if (data.success && status === 200) {
				// Add success message..
				showMessage(tl, form, null, 'success');
				removeSubmitting(tl, form, 1);
				updateSnaptcha(form);
			} else {
				// Get the bad news
				const { errors, message } = data;
				// Show general error(s)..
				if (errors && errors.message[0]) {
					showMessage(tl, form, errors.message[0]);
					// .. or specific message if returned as string
				} else if (message && typeof message === 'string') {
					// From Mailchimp
					showMessage(tl, form, message);
				}
				// show form again
				removeSubmitting(tl, form);
			}
		})
		.catch(error => {
			console.error('catch', error); // eslint-disable-line
			removeSubmitting(tl, form);
			showMessage(tl, form, error);
			validate.isSubmitting = false; // eslint-disable-line
		});
}
