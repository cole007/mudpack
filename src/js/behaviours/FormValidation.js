import { withPlugins, domEvents } from '@spon/plugins';
import validator from '@/ui/validator';

import {
	onInputError,
	onInputValid,
	onSubmitValid,
} from '@/utils/formHelpers.js';

function formValidation({ node }) {
	let validate = null;

	// Setup initial Validation, add event Listeners
	const form = node;
	const { id: namespace } = node; // VERY important, due to multi forms per page
	validate = validator({ form, namespace, fullMessages: false });
	form.setAttribute('novalidate', true); // Graceful

	// Events
	validate.on('input/valid', field => onInputValid(field)); // passed item
	validate.on('input/error', field => onInputError(field)); // passed item & message

	validate.on('submit/valid', e => onSubmitValid(e, form, validate)); // passed event

	return () => {};
}

export default withPlugins(domEvents)(formValidation);
