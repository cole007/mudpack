import { loadApp, loadModule } from '@spon/core';
import { eventBus } from '@spon/plugins';
import barba from '@barba/core';
import barbaCss from '@barba/css';
import 'lazysizes';
import Header from '@/behaviours/Header';
import Modal from '@/behaviours/Modal';
import SetVh from '@/behaviours/SetVh';
import '@/plugins/logger';
import webfontloader from '@/plugins/webfontloader';
import views from '@/views';
import detectTab from '@/utils/detectTab';
import activeLinks from '@/utils/activeLinks';
import localLinks from '@/utils/localLinks';

if (module.hot) {
	module.hot.accept();
}

webfontloader();

// load from data-behaviours
const app = loadApp(name => import(`./behaviours/${name}`), document.body);

loadModule({
	module: Header,
	id: 'header',
	node: document.querySelector('header'),
	keepAlive: true,
});

loadModule({
	module: Modal,
	id: 'modal',
	node: document.body,
	keepAlive: true,
});

loadModule({
	module: SetVh,
	id: 'set-vh',
	node: document.body,
	keepAlive: true,
});

detectTab();

// Avoid "blank page" on JS error
try {
	barba.use(barbaCss);

	barba.hooks.leave(() => {
		app.destroy();
		eventBus.emit('page:exited');
	});

	barba.hooks.after(({ next }) => {
		app.hydrate(next.container);
	});

	barba.hooks.beforeEnter(({ next }) => {
		window.scroll(0, 0);
		eventBus.emit('page:entered', next);

		const {
			url: { path },
			namespace,
		} = next;
		const newPath = path || window.location.origin;
		const segments = newPath.split('/');
		const firstSegment = segments[1].length ? segments[1] : 'home';

		document.documentElement.setAttribute('data-section', firstSegment);
		document.documentElement.setAttribute('data-segments', segments.length - 1);

		document.body.dataset.namespace = namespace;
		activeLinks(newPath);
		localLinks(next.html);
	});

	barba.init({
		debug: true,
		transitions: views,
		timeout: 10000,
		prefetchIgnore: true,
	});
} catch (err) {
	// console.error(err)
}
